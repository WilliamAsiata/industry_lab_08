package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.*;

public class MyReader {

    public void start() {

        // Prompt the user for a file name, then read and print out all the text in that file.
        // Use a BufferedReader.

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        File file = new File(fileName);

        try (BufferedReader bR = new BufferedReader(new FileReader(file))){
            for (String s = bR.readLine(); s != null; s = bR.readLine()) System.out.println(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}

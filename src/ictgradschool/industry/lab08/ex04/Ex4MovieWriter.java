package ictgradschool.industry.lab08.ex04;

import ictgradschool.Keyboard;
import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // Implement this with a PrintWriter

        File file = new File(fileName);

        try (PrintWriter pW = new PrintWriter(new FileWriter(file))){

            pW.println(films.length);

            for (Movie film: films) {
                pW.print(film.getName() + ",");
                pW.print(film.getYear() + ",");
                pW.print(film.getLengthInMinutes() + ",");
                pW.println(film.getDirector());
            }

            System.out.println("Movies saved successfully to " + fileName + "!");

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }


    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}

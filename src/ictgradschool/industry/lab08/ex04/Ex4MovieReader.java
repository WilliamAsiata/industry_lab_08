package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // Implement this with a Scanner

        File file = new File(fileName);

        try (Scanner scanner = new Scanner(file)){

            scanner.useDelimiter(",|\\r\\n");

            int len = scanner.nextInt();
            Movie[] films = new Movie[len];

            for(int i = 0; i < len; i++){
                String name = scanner.next();
                int year = scanner.nextInt();
                int lengthInMinutes = scanner.nextInt();
                String director = scanner.next();

                films[i] = new Movie(name, year, lengthInMinutes, director);
            }

            System.out.println("Movies loaded successfully from " + fileName + "!");

            return films;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}

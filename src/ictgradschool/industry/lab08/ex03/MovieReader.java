package ictgradschool.industry.lab08.ex03;

import ictgradschool.Keyboard;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by anhyd on 20/03/2017.
 */
public class MovieReader {

    public void start() {


        // Get a file name from the user
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();

        // Load the movie data
        Movie[] films = loadMovies(fileName);

        // Do some stuff with the data to check that its working
        printMoviesArray(films);
        Movie mostRecentMovie = getMostRecentMovie(films);
        Movie longestMovie = getLongestMovie(films);
        printResults(mostRecentMovie, longestMovie);
        System.out.println();
        printDirector("Searching for Sugar Man", films);
        printDirector("Liberal Arts", films);
        printDirector("The Intouchables", films);

    }

    /**
     * Reads movies from a file.
     *
     * @param fileName
     * @return
     */
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this method
        File file = new File(fileName);
//        int i = 0;

        /*try (DataInputStream dIn = new DataInputStream(new FileInputStream(file))) {

            while(dIn.available() > 0){
                readNextFilm(dIn);
//                System.out.println(++i);
                i++;
            }

        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }

        Movie[] films = new Movie[i];*/

        try (DataInputStream dIn = new DataInputStream(new FileInputStream(file))){

            int len = dIn.readInt();
            Movie[] films = new Movie[len];

            for(int i = 0; i < len; i++) films[i] = readNextFilm(dIn);

            System.out.println("Movies loaded successfully from " + fileName + "!");

            return films;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Movie readNextFilm(DataInputStream dIn) throws IOException {
        String name = dIn.readUTF();
        int year = dIn.readInt();
        int lengthInMinutes = dIn.readInt();
        String director = dIn.readUTF();

        return new Movie(name, year, lengthInMinutes, director);
    }

    private void printMoviesArray(Movie[] films) {
        System.out.println("Movie Collection");
        System.out.println("================");
        // Step 2.  Complete the printMoviesArray() method
        for(int i = 0; i<films.length; i++){
            System.out.println(films[i].toString());
        }
    }
    private Movie getMostRecentMovie(Movie[] films) {
        // Step 3.  Complete the getMostRecentMovie() method.
        Movie temp=null;
        for(int i = 1; i<films.length; i++){
            if(films[i].isMoreRecentThan(films[i-1])){
                temp = films[i];
            }
        }
        return temp;
    }
    private Movie getLongestMovie(Movie[] films) {
        // Step 4.  Complete the getLongest() method.
        Movie temp=null;
        for(int i = 1; i<films.length; i++){
            if(films[i].isLongerThan(films[i-1])){
                temp = films[i];
            }
        }
        return temp;
    }
    private void printResults(Movie mostRecent, Movie longest) {
        System.out.println();
        System.out.println("The most recent movie is: " + mostRecent.toString());
        System.out.println("The longest movie is: " + longest.toString());
    }
    private void printDirector(String movieName, Movie[] movies) {
        // Step 5. Complete the printDirector() method
        for(int i = 0; i<movies.length; i++){
            if(movieName.equals(movies[i].getName())){
                System.out.println(movieName + " was directed by " + movies[i].getDirector());
                return;
            }
        }
        System.out.println(movieName + " is not in the collection.");
    }

    public static void main(String[] args) {
        new MovieReader().start();
    }
}

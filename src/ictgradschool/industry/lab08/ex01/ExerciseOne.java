package ictgradschool.industry.lab08.ex01;

import com.sun.deploy.util.StringUtils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        fileReaderEx01();

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();
    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // Read input2.txt and print the total number of characters, and the number of e and E characters.
        // Use a FileReader.

        int num = 0;

        FileReader fR = null;
        try {
            fR = new FileReader("input2.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        do {
            try {
                num = fR.read();
//                System.out.println(num);

                if (num == 13) {
                    total--;
                } else if (num != -1){
                    total++;
                }

                if (num == 101 || num == 69){
                    numE++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (num != -1);

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // Read input2.txt and print the total number of characters, and the number of e and E characters.
        // Use a BufferedReader.

        BufferedReader bR = null;

        try {
            bR = new BufferedReader(new FileReader("input2.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String s = "";

        do {
            int len = s.length();

            total += len;
            numE += len - s.replace("E","").replace("e","").length();

            try {
                s = bR.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } while (s != null);

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void fileReaderEx01() {
        int num = 0;
        FileReader fR = null;
        try {
            fR = new FileReader("input1.txt");
            num = fR.read();
            System.out.println(num);
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            System.out.println(fR.read());
            fR.close();
        } catch(IOException e) {
            System.out.println("IO problem");
        }
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
